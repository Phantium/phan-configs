phan-configs
==========

Phan is short for Phantium.  
Configs that I use for my programs, so that I can easily find them and share them with others.

tmux
-----
My tmux configuration

bashrc
-----
My bashrc
