tmux
=====

tmux.conf
-----
My tmux configuration file

MobaXterm
-----
I currently use MobaXterm as my terminal emulator.  
With this, I use the following terminal settings:  

**Font:** [Ubuntu Mono Derivative Powerline](fonts/UbuntuMono/Ubuntu%20Mono%20derivative%20Powerline.ttf)  
**Size:** 12  
**Font aliasing:** Enabled

fonts
-----
This folder currently contains the open source font UbuntuMono for use with powerline.  
The font is as provided by https://github.com/Lokaltog/powerline-fonts
